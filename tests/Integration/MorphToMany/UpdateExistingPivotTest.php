<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration\MorphToMany;

use Altek\Eventually\Tests\Database\Factories\AwardFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\Award;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class UpdateExistingPivotTest extends EventuallyTestCase
{
    /**
     * @return array
     */
    public static function updateExistingPivotProvider(): array
    {
        return [
            [
                // Results
                1,

                // Id
                1,

                // Attributes
                [
                    'prize' => 128,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 128,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                [
                    2,
                ],

                // Attributes
                [
                    'prize' => 1024,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 1024,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                [
                    2,
                    1,
                ],

                // Attributes
                [
                    'prize' => 32768,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 32768,
                            'award_id'       => 2,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 32768,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                Model::class,

                // Attributes
                [
                    'prize' => 16384,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 16384,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],
        ];
    }

    #[Test]
    public function itSuccessfullyRegistersEventListeners(): void
    {
        User::updatingExistingPivot(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 4096,
                    'award_id'       => 1,
                ],
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 4096,
                    'award_id'       => 2,
                ],
            ], $properties);
        });

        User::existingPivotUpdated(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 4096,
                    'award_id'       => 1,
                ],
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 4096,
                    'award_id'       => 2,
                ],
            ], $properties);
        });

        $user = UserFactory::new()->create();

        $awards = AwardFactory::new()->count(2)->create()->each(static function (Award $award) use ($user): void {
            $award->users()->attach($user);
        });

        self::assertCount(2, $user->awards()->get());

        self::assertSame(2, $user->awards()->updateExistingPivot($awards, [
            'prize' => 4096,
        ]));
    }

    #[Test]
    public function itPreventsExistingPivotFromBeingUpdated(): void
    {
        User::updatingExistingPivot(static function () {
            return false;
        });

        $user = UserFactory::new()->create();

        $awards = AwardFactory::new()->count(2)->create()->each(static function (Award $award) use ($user): void {
            $award->users()->attach($user);
        });

        self::assertCount(2, $user->awards()->get());

        self::assertFalse($user->awards()->updateExistingPivot($awards, [
            'prize' => 256,
        ]));

        self::assertCount(2, $user->awards()->get());
    }

    /**
     * @param int   $results
     * @param mixed $id
     * @param array $attributes
     * @param array $expectedPayload
     */
    #[Test]
    #[DataProvider('updateExistingPivotProvider')]
    public function itSuccessfullyUpdatesExistingPivot(int $results, $id, array $attributes, array $expectedPayload): void
    {
        $user = UserFactory::new()->create();

        $awards = AwardFactory::new()->count(2)->create()->each(static function (Award $award) use ($user): void {
            $award->users()->attach($user);
        });

        self::assertCount(2, $user->awards()->get());

        Event::fake();

        switch ($id) {
            case Model::class:
                $id = $awards->first();
                break;

            case Collection::class:
                $id = $awards;
                break;
        }

        self::assertSame($results, $user->awards()->updateExistingPivot($id, $attributes));

        Event::assertDispatched(sprintf('eloquent.updatingExistingPivot: %s', User::class), static function ($event, $payload, $halt) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            self::assertTrue($halt);

            return true;
        });

        Event::assertDispatched(sprintf('eloquent.existingPivotUpdated: %s', User::class), static function ($event, $payload) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            return true;
        });
    }
}
