<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration\BelongsToMany;

use Altek\Eventually\Tests\Database\Factories\ArticleFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class SyncTest extends EventuallyTestCase
{
    /**
     * @return array
     */
    public static function syncProvider(): array
    {
        return [
            [
                // Results
                [
                    'attached' => [
                        1,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                1,

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                [
                    2,
                ],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                        1,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                [
                    2 => [
                        'liked' => false,
                    ],
                    1 => [
                        'liked' => true,
                    ],
                ],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'liked'      => false,
                            'article_id' => 2,
                        ],
                        [
                            'user_id'    => 1,
                            'liked'      => true,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        1,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                Model::class,

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        1,
                        2,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                Collection::class,

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                        [
                            'user_id'    => 1,
                            'article_id' => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        1,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                BaseCollection::make(1),

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                        1,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                BaseCollection::make([
                    2,
                    1,
                ]),

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 2,
                        ],
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                    ],
                    'detached' => [],
                    'updated'  => [],
                ],

                // Id
                BaseCollection::make([
                    2 => [
                        'liked' => false,
                    ],
                ]),

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'liked'      => false,
                            'article_id' => 2,
                        ],
                    ],
                ],
            ],
        ];
    }

    #[Test]
    public function itSuccessfullyRegistersEventListeners(): void
    {
        User::syncing(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('articles', $relation);

            self::assertSame([
                [
                    'user_id'    => 1,
                    'article_id' => 1,
                ],
            ], $properties);
        });

        User::synced(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('articles', $relation);

            self::assertSame([
                [
                    'user_id'    => 1,
                    'article_id' => 1,
                ],
            ], $properties);
        });

        $user    = UserFactory::new()->create();
        $article = ArticleFactory::new()->create();

        self::assertCount(0, $user->articles()->get());

        self::assertSame([
            'attached' => [
                1,
            ],
            'detached' => [],
            'updated'  => [],
        ], $user->articles()->sync($article));

        self::assertCount(1, $user->articles()->get());
    }

    #[Test]
    public function itPreventsModelsFromBeingSynced(): void
    {
        User::syncing(static function () {
            return false;
        });

        $user     = UserFactory::new()->create();
        $articles = ArticleFactory::new()->count(2)->create();

        self::assertCount(0, $user->articles()->get());

        self::assertFalse($user->articles()->sync($articles));

        self::assertCount(0, $user->articles()->get());
    }

    /**
     * @param array $results
     * @param mixed $id
     * @param array $expectedPayload
     */
    #[Test]
    #[DataProvider('syncProvider')]
    public function itSuccessfullySyncsModels(array $results, $id, array $expectedPayload): void
    {
        $user     = UserFactory::new()->create();
        $articles = ArticleFactory::new()->count(2)->create();

        self::assertCount(0, $user->articles()->get());

        Event::fake();

        switch ($id) {
            case Model::class:
                $id = $articles->first();
                break;

            case Collection::class:
                $id = $articles;
                break;
        }

        self::assertSame($results, $user->articles()->sync($id));

        Event::assertDispatched(sprintf('eloquent.syncing: %s', User::class), static function ($event, $payload, $halt) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            self::assertTrue($halt);

            return true;
        });

        Event::assertDispatched(sprintf('eloquent.synced: %s', User::class), static function ($event, $payload) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            return true;
        });
    }
}
