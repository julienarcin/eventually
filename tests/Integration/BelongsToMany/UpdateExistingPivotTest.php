<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration\BelongsToMany;

use Altek\Eventually\Tests\Database\Factories\ArticleFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\Article;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class UpdateExistingPivotTest extends EventuallyTestCase
{
    /**
     * @return array
     */
    public static function updateExistingPivotProvider(): array
    {
        return [
            [
                // Results
                1,

                // Id
                1,

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                [
                    2,
                ],

                // Attributes
                [
                    'liked' => false,
                ],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'liked'      => false,
                            'article_id' => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                [
                    2,
                    1,
                ],

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 2,
                        ],
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                Model::class,

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                Collection::class,

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                        [
                            'user_id'    => 1,
                            'article_id' => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                BaseCollection::make(1),

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                BaseCollection::make([
                    2,
                    1,
                ]),

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'articles',
                    2 => [
                        [
                            'user_id'    => 1,
                            'article_id' => 2,
                        ],
                        [
                            'user_id'    => 1,
                            'article_id' => 1,
                        ],
                    ],
                ],
            ],
        ];
    }

    #[Test]
    public function itSuccessfullyRegistersEventListeners(): void
    {
        User::updatingExistingPivot(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('articles', $relation);

            self::assertSame([
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 1,
                ],
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 2,
                ],
            ], $properties);
        });

        User::existingPivotUpdated(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('articles', $relation);

            self::assertSame([
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 1,
                ],
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 2,
                ],
            ], $properties);
        });

        $user = UserFactory::new()->create();

        $articles = ArticleFactory::new()->count(2)->create()->each(static function (Article $article) use ($user): void {
            $article->users()->attach($user);
        });

        self::assertCount(2, $user->articles()->get());

        self::assertSame(2, $user->articles()->updateExistingPivot($articles, [
            'liked' => true,
        ]));
    }

    #[Test]
    public function itPreventsExistingPivotFromBeingUpdated(): void
    {
        User::updatingExistingPivot(static function () {
            return false;
        });

        $user = UserFactory::new()->create();

        $articles = ArticleFactory::new()->count(2)->create()->each(static function (Article $article) use ($user): void {
            $article->users()->attach($user);
        });

        self::assertCount(2, $user->articles()->get());

        self::assertFalse($user->articles()->updateExistingPivot($articles, [
            'liked' => true,
        ]));

        self::assertCount(2, $user->articles()->get());
    }

    /**
     * @param int   $results
     * @param mixed $id
     * @param array $attributes
     * @param array $expectedPayload
     */
    #[Test]
    #[DataProvider('updateExistingPivotProvider')]
    public function itSuccessfullyUpdatesExistingPivot(int $results, $id, array $attributes, array $expectedPayload): void
    {
        $user = UserFactory::new()->create();

        $articles = ArticleFactory::new()->count(2)->create()->each(static function (Article $article) use ($user): void {
            $article->users()->attach($user);
        });

        self::assertCount(2, $user->articles()->get());

        Event::fake();

        switch ($id) {
            case Model::class:
                $id = $articles->first();
                break;

            case Collection::class:
                $id = $articles;
                break;
        }

        self::assertSame($results, $user->articles()->updateExistingPivot($id, $attributes));

        Event::assertDispatched(sprintf('eloquent.updatingExistingPivot: %s', User::class), static function ($event, $payload, $halt) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);


            self::assertTrue($halt);

            return true;
        });

        Event::assertDispatched(sprintf('eloquent.existingPivotUpdated: %s', User::class), static function ($event, $payload) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            return true;
        });
    }
}
