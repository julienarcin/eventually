<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Unit;

use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\User;
use PHPUnit\Framework\Attributes\Test;

class HasEventsTest extends EventuallyTestCase
{
    #[Test]
    public function itAssuresThePivotEventsAreObservable(): void
    {
        $user = new User();

        self::assertContains('toggling', $user->getObservableEvents());
        self::assertContains('toggled', $user->getObservableEvents());
        self::assertContains('syncing', $user->getObservableEvents());
        self::assertContains('synced', $user->getObservableEvents());
        self::assertContains('updatingExistingPivot', $user->getObservableEvents());
        self::assertContains('existingPivotUpdated', $user->getObservableEvents());
        self::assertContains('attaching', $user->getObservableEvents());
        self::assertContains('attached', $user->getObservableEvents());
        self::assertContains('detaching', $user->getObservableEvents());
        self::assertContains('detached', $user->getObservableEvents());
    }
}
